package tp3Swing;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class UneApplicationGraphiqueActive extends JFrame implements ActionListener{
	private Container panneau;
	private JButton monBouton;
	private JTextField monChampTexte;
	private JLabel monLabel;

	public UneApplicationGraphiqueActive(){
		//Cr�ation de la fen�tre
		super("Mon Application Graphique");
		setSize(300, 150);
		setLocation(20,20);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//R�cup�ration du container
		panneau = getContentPane();
		panneau.setLayout(new GridLayout(3,1));

		//Cr�ation des objets � utiliser
		monLabel = new JLabel("Mon label");
		monChampTexte = new JTextField("Saisir ici");
		monBouton = new JButton("Mon Bouton");

		//Ajout au panneau
		panneau.add(monLabel);
		panneau.add(monChampTexte);
		panneau.add(monBouton);
		
		monBouton.addActionListener(this);

		setVisible(true);
	}

	public static void main(String[] args) {
		UneApplicationGraphiqueActive n = new UneApplicationGraphiqueActive();
	}

	public void traiterMonBouton(){
		System.out.println("Click");
	}

	public void actionPerformed(ActionEvent e) {
		traiterMonBouton();
	}
}

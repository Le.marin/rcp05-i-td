package tp3Swing;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Convertisseur extends JFrame implements ActionListener {
	//Attributs
	private Container panneau;
	private Double tva, ht;
	private JTextField textetva, texteht, textettc;
	private JButton calculer;
	
	//Methodes
	public Convertisseur(){
		//Cr�ation de la fen�tre
		super("Convertisseur");
		setSize(350, 150);
		setLocation(20,20);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//R�cup�ration du container
		panneau = getContentPane();
		panneau.setLayout(new GridLayout(3, 4));

		//Cr�ation des objets � utiliser
		JPanel a = new JPanel(new GridLayout(1,1));
		a.add(new JLabel("Montant TVA"));
		a.add(textetva = new JTextField(""));

		JPanel b = new JPanel(new GridLayout(1,4));
		b.add(new JLabel("Montant HT"));
		b.add(texteht = new JTextField(""));
		b.add(new JLabel("Montant TTC"));
		b.add(textettc = new JTextField(""));

		//Ajout au panneau
		panneau.add(a);
		panneau.add(b);
		panneau.add(calculer = new JButton("Calculer!"));
		
		//Permet d'ajouter au bouton calculer le pouvoir de faire une action
		calculer.addActionListener(this);
		
		//Permet de rendre visible ou invisible la fenetre
		setVisible(true);
	}
	
	//M�thode permettant de calculer le ttc � partir de ht et tva
	public void versTTC (Double ht, Double tva) {
		Double ttc = ht + (ht * (tva/100));
		textettc.setText("" + ttc);
	}

	//Methode Main
	public static void main(String[] args) {
		Convertisseur c = new Convertisseur();
	}

	//Cette Methode permet de r�aliser l'action gr�ce � la m�thode du calcul du ttc
	@Override
	public void actionPerformed(ActionEvent e) {
		tva = Double.parseDouble(textetva.getText());
		ht = Double.parseDouble(texteht.getText());
		this.versTTC(ht, tva);
	}
}
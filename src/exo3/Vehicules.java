package exo3;

public abstract class Vehicules {
	//Attributs
	public int id;
	public int cpt = 0;
	public boolean enPanne = false;
	
	//Methodes
	public void avancer() {
		System.out.println("J'avance de ...");
	}
	
	public void afficherNbKm() {
		System.out.println("J'ai ...");
	}
	
	public abstract void sePresenter();

}

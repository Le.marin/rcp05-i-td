package tp1;

public class Stuart extends Minion {
	//Attributs
	
	private String nom = "Stuart"; //Declaration du nom, de l'id, de l'economie
	public static int id = 0; 			   // de ses annees restantes avant sa retraire ainsi que la capacite de production et son salaire
	private int nbPieces = 0;
	public int nbAnneesRestantes;
	private int cptDeProd;
	private int salaire;
	
	//Constructeurs
	public Stuart() {
		id = id + 1;
		nbAnneesRestantes= 6 + (int)(Math.random() * ((10 - 6) +1)); //Calcul al�atoire de ses annees restantes pour prendre sa retraite
		cptDeProd= 15 + (int)(Math.random() * ((25 - 15) +1)); //Calcul al�atoire de sa capacite de production
	}
	
	//M�thodes
	public int getEconomies() {
		return nbPieces;
	}
	
	void sePresenter() { //Methode permettant au Stuart de se presenter
		System.out.println(nom + " " + id + " :j'ai " + nbPieces + " grosse(s) pi�ce(s) en bourse et " + nbAnneesRestantes + " ann�es avant la retraite ");
	}
	
	void travailler() { //Methode permettant au Stuart de travailler et gagner son salaire 
		
		sePresenter();
		if (nbAnneesRestantes > 0) {
			//Calculer le salaire
			salaire = cptDeProd * (100 + nbPieces) /100;
			//Economiser le salaire du Stuart
			nbPieces = salaire + nbPieces;
			//Il perd une annee chaque annees
			nbAnneesRestantes = nbAnneesRestantes - 1;
			
			System.out.println(nom + " " + id + " :J'ai eu cette ann�e " + salaire + " grosses pi�ces ce qui me fait " + nbPieces + " grosses pi�ces, il me reste " + nbAnneesRestantes + " ans pour prendre ma retraite");
			sePresenter();
		}
		else {
			
			System.out.println("Je suis enfin � la retraite !");
		}
		
		System.out.println(" ");
	}
}

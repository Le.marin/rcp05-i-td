package tp1;

import java.util.Vector; //Importe le package necessaire � la r�alisation de l'exerice

public class Usine {
	//Attributs
	Vector <Stuart> listeDesStuarts = new Vector<Stuart>(); //Creation d'un vecteur Stuart
	int numero;
	private double coffre; //D�claration d'un coffre

	
	//Constructeurs
	public Usine() {
		coffre = 0; //Coffre initialis� � 0;
		System.out.println("La cloche sonne, tout le monde au travail !");
		System.out.println(" ");
		System.out.println("Le coffre a: " + coffre + " grosses pi�ces");
		
		System.out.println(" ");
	}
	
	public void recruterStuart() { //Cette m�thode permet d'ajouter les Stuarts � la liste des ouvriers
		Stuart s = new Stuart(); //Declaration de 2 Stuarts
		this.listeDesStuarts.add(s);
		Stuart s1 = new Stuart(); 
		this.listeDesStuarts.add(s1);
	}
	
	public void sonnerCloche() { //Cette m�thode permet au Stuart de travailler	
		for (int numero = 0; numero < listeDesStuarts.size(); numero++) {
			listeDesStuarts.get(numero).travailler();
		}
		
		int nbPieces = listeDesStuarts.get(numero).getEconomies(); 
		
		double impotRetraite = nbPieces * 0.2; //Calcul de l'impot de la Retraite, prendre 20% de l'economie des Stuarts
		coffre = impotRetraite + coffre; //Ajout des economies dans le coffre de l'usine
	}

	public void coffreUsine() { //Cette m�thode affiche l'etat du coffre de l'usine
		System.out.println("Le coffre de l'usine a: " + coffre + " grosses pi�ces");
	}

	
	
	
	/*public void stuartLicencier() {			//En cours de d�veloppement
		this.listeDesStuarts.remove(s);
	}*/
	

	
	
	
	 
	 
	 
	 
	
}

package tp6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;


public class ChatServeur extends JFrame implements ActionListener {

	/*___________ Attributs ______________*/
	private JTextArea panneauAffichage;		//Zone d'affichage
	private JButton b_Exit;					//Bouton exit
	private ServerSocket socketServeur;			
	private int numeroPort = 8888;
	private BufferedReader readerServeur;	//Objet permettant la lecture des messages re�us sur le socket
	private PrintWriter writerServeur;
	
	/*___________ Constructeur ______________*/
	public ChatServeur(){
		//D�finition de la fen�tre
		super("Serveur - Panneau d'affichage");
		setSize(400, 300);
		setLocation(200,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Cr�ation des composants graphiques
		panneauAffichage = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(panneauAffichage, 
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		panneauAffichage.setEditable(false);
		b_Exit = new JButton("Exit");
		b_Exit.addActionListener(this); 

		// Ajout des composants graphique � la fen�tre
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(scrollPane, BorderLayout.CENTER);
		pane.add(b_Exit, BorderLayout.SOUTH);

		//Affichage d'un message de bienvenue
		afficherPanneau("Le panneau est actif\n");

		//Cr�ation du ServeurSocket
		try {
			socketServeur = new ServerSocket(numeroPort);
			afficherPanneau("Serveur d�marr�\n");

		} catch (Exception e) {
			afficherPanneau("Erreur de cr�ation ServerSocket \n");
		}

		//Affichage de la fen�tre
		setVisible(true);

		// D�mmarage de l'�coute 
		ecouter();				
	}	


	/*___________ M�thodes ______________*/

	// D�marrage de l'�coute
	private void ecouter() {

		try {
			afficherPanneau("Serveur en attente de connexion...\n");

			//Attente d'une connection client - Bloqu� par la methode accept jusqu'� la connexion d'un client.
			//Lorsqu'un client se connecte, sortie de l'attente pour la cr�ation d'un socket
			Socket clientConnecte = socketServeur.accept();
			afficherPanneau("Client connect�\n");

			//Les messages re�us sont format�s et stock�s dans une m�moire tampon 
			readerServeur = new BufferedReader(new InputStreamReader(clientConnecte.getInputStream()));
			writerServeur = new PrintWriter(clientConnecte.getOutputStream()); 
			
			//On lit et affiche la chaine de caract�re
			String ligne; 
			while ((ligne = readerServeur.readLine()) != null){ 
				afficherPanneau("Message recu : " + ligne + "\n");
				writerServeur.println(ligne);
				writerServeur.flush();
			}

		} catch (Exception e) {
			afficherPanneau("Connexion Termin�e");
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Fermeture de l'application sur action du bouton Exit 
		if (e.getSource() == b_Exit) {
			System.exit(-1);
		}
	}
	
	public void afficherPanneau(String str){
		panneauAffichage.append(str);
	}

	/*___________ M�thode Main ______________*/
	public static void main (String[] args){
		new ChatServeur();

	}

}

package td3;

public class HelloworldImplementsRunnable implements Runnable{
	protected String nom;
	
	public HelloworldImplementsRunnable(String nom) {
		this.nom = nom;
	}
	
	public void run() {   
		for (int i = 1; i <= 10; i++) {    
			try {     
				Thread.sleep(10);    
			}     
			catch(InterruptedException e) {     
				System.err.println(nom + " a �t� interrompu.");    
			}   
		System.out.println(nom);   
		} 
	}
	
	public static void main(String[] args) {   
		HelloworldImplementsRunnable hello = new HelloworldImplementsRunnable("Hello"); 
		Thread t1= new Thread(hello);     
		t1.start();
		
		HelloworldImplementsRunnable world = new HelloworldImplementsRunnable("World"); 
		Thread t2= new Thread(world);     
		t2.start();

	}

}

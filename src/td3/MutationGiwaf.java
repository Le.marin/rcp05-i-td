package td3;

public interface MutationGiwaf {
	public abstract int monNombreDYeux();
	public abstract String maForme();
	// retourne "indefini" si forme changeante 
	public abstract String maCouleur(); 
	// retourne "indefini" si couleur changeante 
	
	public abstract int vitessedeDeplacement() throws NeSeDeplacePasException;
}

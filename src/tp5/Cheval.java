package tp5;

public class Cheval extends Thread implements CoureurHippique {
	//Attributs
	private int id = 0;
	private int position = 0;
	private JugeDeCourse monJuge;
	private int longueurPiste;
	private int distanceParcourue;
	
	//Constructeurs
	public Cheval(int i, int lP, JugeDeCourse j) {
		id = i;
		longueurPiste = lP;
		monJuge = j;
		
	}
	
	public void run() {
		//D�part
		distanceParcourue = 0;
		//Course
		while (distanceParcourue < longueurPiste) {
			distanceParcourue = distanceParcourue + 1;
			System.out.println("Le cheval " + id + " a parcourue " + distanceParcourue + " m !");
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//Arriv�e
		monJuge.passeLaLigneDArrivee(id);
	}
	
	//M�thodes
	@Override
	public int distanceParcourue() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	//Accesseurs
	
	
}

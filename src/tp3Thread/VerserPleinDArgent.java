package tp3Thread;

public class VerserPleinDArgent implements Runnable {
	//Attributs
	private Compte account;
	
	
	//Constructeurs
	public VerserPleinDArgent(Compte c) {
		account = c;
	}

	
	//Methodes
	public void run() {
		for (int i = 1; i<= 100; i++) {
			account.verserArgent(2);
			account.afficherSolde();
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	//Accesseurs



}

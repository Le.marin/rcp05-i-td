package tp3Thread;

public class Compteur1bis implements Runnable {

	private String nom;

	public Compteur1bis(String nom) {
		this.nom=nom;
	}


	public void run() {
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}
	

	public static void  main(String args[]) throws InterruptedException{
		Compteur1bis t1, t2, t3;
		t1=new Compteur1bis("Hello ");
		t2=new Compteur1bis("World ");
		t3=new Compteur1bis("and Everybody ");
		Thread thread1 = new Thread(t1);
		Thread thread2 = new Thread(t2);
		Thread thread3 = new Thread(t3);

		thread1.start();
		thread2.start();
		thread3.start();
		//Pause de 100ms apr�s le lancement du 3�me Thread
		Thread.sleep(100); 

		System.out.println("Fin du programme");
		
		System.exit(0);

	}
}

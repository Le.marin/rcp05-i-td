package tp3Thread;

public class Banque {

	public static void main(String[] args) {
		Compte c1 = new Compte();
		c1.afficherSolde();
		c1.verserArgent(2);
		
		Compte c2 = new Compte();
		VerserPleinDArgent v1 = new VerserPleinDArgent(c2);
		Thread t1 = new Thread(v1);
		
	
		VerserPleinDArgent v2 = new VerserPleinDArgent(c2);
		Thread t2 = new Thread(v2);
		
		t1.start();
		t2.start();
	}

}

package tp3Thread;

public class Compteur1 extends Thread {

	private String nom;

	public Compteur1(String nom) {
		this.nom=nom;
	}


	public void run() {
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}

	public static void  main(String args[]) throws InterruptedException{
		Compteur1 t1, t2, t3;
		t1=new Compteur1("Hello ");
		t2=new Compteur1("World ");
		t3=new Compteur1("and Everybody ");

		t1.start();
		t2.start();
		t3.start();
		//Pause de 100ms apr�s le lancement du 3�me Thread
		Thread.sleep(100); 

		System.out.println("Fin du programme");
		
		System.exit(0);

	}

}

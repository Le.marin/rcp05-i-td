package tp4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LaFourmiDeLangton extends JFrame implements ActionListener {
	//Attributs
	private int dimension = 10;
	private int nbTours = 0;
	private Container panneau;
	private JTextField plateau[][];
	private JButton next;
	
	//Constructeurs
	public LaFourmiDeLangton() {
		//Cr�ation de la fen�tre
		super("La Fourmi de Langton");
		setSize(500, 500);
		setLocation(300,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//R�cup�ration du container
		panneau = getContentPane();
		this.setLayout(new BorderLayout());
		
				
		//Cr�ation des objets � utiliser et ajout sur le container
		panneau.add(new JLabel("Nombres de tours = " + nbTours), BorderLayout.NORTH);
		panneau.add(next = new JButton("Next"), BorderLayout.SOUTH);
		panneau.add(creerPlateau(), BorderLayout.CENTER);
		
		//Permet d'ajouter au bouton calculer le pouvoir de faire une action
		next.addActionListener(this);
		
		//Permet de rendre visible ou non la fen�tre java.
		setVisible(true);
	}
	
	//M�thodes
	public JPanel creerPlateau() {
		JPanel p = new JPanel();
		plateau = new JTextField[dimension][dimension];
		
		p.setLayout(new GridLayout(dimension,dimension));
		for(int i = 0; i < dimension; i++) {
			for(int j = 0; j < dimension; j++) {
				JTextField s = new JTextField();
				plateau[i][j]=s;
				p.add(s);
			}
		}
		//plateau[4][5].setBackground(Color.BLACK); 
		return p;
	}
	
	public void initFourmi() {
		Fourmi fourmi = new Fourmi();
		fourmi.x= dimension/2;
		fourmi.y= dimension/2;
		plateau[fourmi.x][fourmi.y].setText("" + fourmi.orientation);
	}
	
	public void next() {
		/*if(plateau[fourmi.x][fourmi.y].getBackground()  == Color.white){
			
		}*/
	}
	
	//Affichage
	public static void main(String[] args) {
		LaFourmiDeLangton f = new LaFourmiDeLangton();
		f.initFourmi();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}
	

}

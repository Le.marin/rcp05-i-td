package tp2;

public class FruitEnVrac extends Fruits{
	//Attributs
	
	//Constructeurs
	public FruitEnVrac(String n, float pa, float pv, float qs, String po) { //Appelle du constructeur de la classe m�re en utilisant super()
		super(n, pa, pv, qs, po);
	}

	//Methodes
	public void sePresenter() { //Cette m�thode permet de presenter le produit en donnant son nom, le prix de vente et le stock
		System.out.println(nom + ": Le prix est � " + prixVente + "�, il en reste " + quantiteFruits + " kg");
		System.out.println(" ");
	}
	
	public void augmenterStock(float q) { //Permet d'augmenter le stock du produit en question
		System.out.println("Vous avez re�u: " + q + " " +nom);
		System.out.println(" ");
		quantiteFruits = quantiteFruits + q; //Calcul qui permet d'augmenter le stock
		sePresenter();
	}
	
	public void diminuerStock(float q) { //Permet de diminuer le stock du produit en question
		System.out.println("Vous avez enlev�: " + q + " kg de " + nom);
		System.out.println(" ");
		quantiteFruits =  quantiteFruits - q;
		System.out.println(nom + ": Le prix est � " + prixVente + "�, il en reste " + quantiteFruits + " kg");
		System.out.println(" ");
	}
	
	public void vendreProduit(float p) { //Cette m�thode permet de vendre le produit et gagner de l'argent
		if (p <= quantiteStock) { //Boucle if/else pour �viter d'acheter plus que le stock
			System.out.println("Un client a achet� " + p + " kg de " + nom + ". Vous avez re�u " + prixVente*p + "�");
			System.out.println(" ");
			quantiteFruits = quantiteFruits - p; //Calcul afin de diminuer le stock apr�s la vente
			coffreMagasin = coffreMagasin + prixVente; //Calcul afin d'augmenter les b�nifices du magasin
			sePresenter();
		}
		else {
			System.out.println("Il est impossible d'acheter plus que le stock !!");
		}
		
		
	}
	
	public void soldeProduit(int s) { //s est la solde en pourcentage
		System.out.println("C'est les soldes ! " + s + "% de remise !");
		float reduc = prixVente * s/100; //Ce calcul permet de convertir la solde de pourcentage en euros
		System.out.println("Vous pouvez �conomiser: " + reduc + "� !!");
		prixVente = prixVente - reduc; //Celui ci permet tout simplement d'afficher la valeur r�elle d'un produit avec la solde
		sePresenter();
	}
	
	//Accesseurs
}
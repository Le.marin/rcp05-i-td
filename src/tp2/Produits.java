package tp2;

public abstract class Produits {
	
	//Attributs
	protected String nom; //D�claration des variables permettant l'identification du produits (nom, prix achat et de vente, la quantit� en stock ..)
	protected float prixAchat;
	protected float prixVente;
	protected int quantiteStock;
	protected float coffreMagasin;
	
	//Constructeurs
	public Produits(String n, float pa, float pv, int qs) { //Ce constructeur permet d'initialiser les differentes variables afin de pouvoir les appeler dans les autres classes
		nom = n;
		prixAchat = pa;
		prixVente = pv;
		quantiteStock = qs;		
	}
	

	//Methodes
	public void sePresenter() { //Cette m�thode permet de presenter le produit en donnant son nom, le prix de vente et le stock
		System.out.println(nom + ": Le prix est � " + prixVente + "�, il en reste " + quantiteStock);
		System.out.println(" ");
	}
	
	//Accesseurs


}

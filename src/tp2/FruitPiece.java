package tp2;

public class FruitPiece extends Fruits {
	//Attributs

	
	//Constructeurs
	public FruitPiece(String n, float pa, float pv, int qs, String po) {
		super(n, pa, pv, qs, po);
	}

	//Methodes
	public void augmenterStock(int q) { //Permet d'augmenter le stock du produit en question
		System.out.println("Vous avez re�u: " + q + " " +nom);
		System.out.println(" ");
		quantiteStock = quantiteStock + q; //Calcul qui permet d'augmenter le stock
		sePresenter();
	}
	
	public void diminuerStock(int q) { //Permet de diminuer le stock du produit en question
		System.out.println("Vous avez enlev�: " + q + " kg de " + nom);
		System.out.println(" ");
		quantiteStock =  quantiteStock - q;
		System.out.println(nom + ": Le prix est � " + prixVente + "�, il en reste " + quantiteStock + " kg");
		System.out.println(" ");
	}

	public void vendreProduit(int u) { //Cette m�thode permet de vendre le produit et gagner de l'argent
		if (u <= quantiteStock) { //Boucle if/else pour �viter d'acheter plus que le stock
			System.out.println("Un client a achet� " + u + " " + nom + ". Vous avez re�u " + prixVente*u + "�");
			System.out.println(" ");
			quantiteStock = quantiteStock - u; //Calcul afin de diminuer le stock apr�s la vente
			coffreMagasin = coffreMagasin + prixVente; //Calcul afin d'augmenter les b�nifices du magasin
			sePresenter();
		}
		else {
			System.out.println("Il est impossible d'acheter plus que le stock !!");
		}
	}
	
	//Accesseurs

}

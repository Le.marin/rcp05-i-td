package tp2;


public class AppareilELec extends Produits {


	//Attributs
	protected int nbColis;
	protected int codeBarre;
	
	//Constructeurs
	public AppareilELec(String n, float pa, float pv, int qs, int id) {
		super(n, pa, pv, qs);
		codeBarre = id;
	}
	
	//Methodes
	public void augmenterStock(int q) { //Permet d'augmenter le stock du produit en question
		System.out.println("Vous avez re�u: " + q + " " +nom);
		System.out.println(" ");
		quantiteStock = quantiteStock + q; //Calcul qui permet d'augmenter le stock
		sePresenter();
		
	}
	
	
	public void diminuerStock(int q) { //Permet de diminuer le stock du produit en question
		System.out.println("Vous avez enlev�: " + q + " kg de " + nom);
		System.out.println(" ");
		quantiteStock =  quantiteStock - q; //Calcul permettant d'afficher la valeur du stock apr�s achat 
		System.out.println(nom + ": Le prix est � " + prixVente + "�, il en reste " + quantiteStock + " kg");
		System.out.println(" ");
	}
	
	public void vendreProduit(int u) { //Cette m�thode permet de vendre le produit et gagner de l'argent
		if (u <= quantiteStock) { //Boucle if/else pour �viter d'acheter plus que le stock
			System.out.println("Un client a achet� " + u + " " + nom + ". Vous avez re�u " + prixVente*u + "�");
			System.out.println(" ");
			quantiteStock = quantiteStock - u; //Calcul afin de diminuer le stock apr�s la vente
			coffreMagasin = coffreMagasin + prixVente; //Calcul afin d'augmenter les b�nifices du magasin
			sePresenter();
		}
		else {
			System.out.println("Il est impossible d'acheter plus que le stock !!");
		}
	}
	
	public void soldeProduit(int s) { //s est la solde en pourcentage
		System.out.println("C'est les soldes ! " + s + "% de remise !");
		float reduc = prixVente * s/100; //Ce calcul permet de convertir la solde de pourcentage en euros
		System.out.println("Vous pouvez �conomiser: " + reduc + "� !!");
		prixVente = prixVente - reduc; //Celui ci permet tout simplement d'afficher la valeur r�elle d'un produit avec la solde
		sePresenter();
	}
	
	//Accesseurs
	
}







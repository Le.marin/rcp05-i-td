package tp2;

public abstract class Fruits extends Produits {
	//Attributs
	protected String paysOrigine; //D�claration des variables n�cessaire � la classe abstraite Fruits
	protected float quantiteFruits;
	
	//Constructeurs
	public Fruits(String n, float pa, float pv, float qs, String po) { //Appelle du constructeur de la classe m�re en utilisant super()
		super(n, pa, pv, (int) qs);
		quantiteFruits = qs;
		paysOrigine = po;
	}
	
	//M�thodes
	
	//Accesseurs
}

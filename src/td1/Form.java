package td1;

public abstract class Form {
	//Attributs
	Point p;
	
	//Methodes
	public void deplacer() {
		System.out.println("Je me d�place");
	}
	
	//Methodes abstraites
	public abstract float calculerSurface();
	public abstract float calculerPerimetre();
	public abstract float seDecrire();

}
